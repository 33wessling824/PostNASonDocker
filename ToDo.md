# ToDo

Hier sollten alle Aufgaben gesammelt werden, welche noch zu erledigen sind.

## Bugs

## Feature
-   [ ] mittels **NodeRed** _(wesentliche)_ Teile der Konfiguration beeinflussen
-   [ ] **NodeRed-GUI** zum UpLoad der NAS-Daten nutzen
-   [ ] Log des Importers mittels der **NodeRed-GUI** anzeigen / zurücksetzen
-   [ ] Backup mit **NodeRed** ermöglichen
-   [ ] auf Basis der im Verzeichnis ```/nas``` liegenden Dateien eine Konfiguration anlegen

## Ideen
-   [ ]   [NodeRed](https://nodered.org) nutzen um Prozesse anzustoßen bzw. eine GUI zu liefern
-   [x]   Scripte erstellen die per ```git``` die NorBit-Lösung holt (v0.2.0)

## Offene Fragen
-   [ ] Werden die folgenden Pakete benötigt?
  - python-qt4
  - libqt4-sql-psql
