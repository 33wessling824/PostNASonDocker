# PostNASonDocker
Docker Images um NAS-Daten nach PostGIS zu importieren.

Mittels `docker-compose` wird eine _Umgebung_ aus zwei Docker-Containern geschaffen!

Der Container 'postnasdb' enthält eine PostGIS-DB Versions 2.5 auf Basis von PostgreSQL 11. Die Datenbank hat den Namen 'alkis'.

Der Container 'postnasconv' ist Basis für den eigentlichen [ALKIS-Importer](https://github.com/norBIT/alkisimport).

## Important!
English information is provided in [this extra readme](README-eng.md)!

## Ziel
Mit dieser Docker Umgebung soll es möglich sein, schnell große Mengen von XML-basierten ALKIS-Daten per [NAS](https://de.wikipedia.org/wiki/Normbasierte_Austauschschnittstelle) einzulesen.

Basis dafür sind die folgenden Projekte:
*   PostGIS
*   ogr2ogr
    *   nas2ogr
*   [https://github.com/norBIT/alkisimport](https://github.com/norBIT/alkisimport)

## Daten
In einigen Bundesländern sind die [ALKIS](https://de.wikipedia.org/wiki/Amtliches_Liegenschaftskatasterinformationssystem)-Daten inzwischen frei zugänglich. Es folgt eine Auflistung der mir bekannten Quellen:

### NRW
* [Landesspezifische Vorgaben zu ALKIS in NRW](https://www.bezreg-koeln.nrw.de/brk_internet/geobasis/liegenschaftskataster/alkis/vorgaben/index.html)
*  Die eigentlichen Daten finden sich unter  [https://www.opengeodata.nrw.de/produkte/geobasis/lika/alkis_sek/bda_oe/](https://www.opengeodata.nrw.de/produkte/geobasis/lika/alkis_sek/bda_oe/)

## Docker-Container erstellen und starten
### erstellen ...
```> docker-compose up -d```

*ggfs. neu erstellen:*

```> docker-compose up -d --build```

### sonstiges
Möchte man auf die ```bash``` des laufenden Imgages zugreigen kann man dies per:

```> docker exec -it postnasondocker_postnasconv_1 bash```

## Release Notes ...
... finden sich [hier](ReleaseNotes.md).
